/*******************************************************************************
*          ALUMNO: GERMAN TORRES SANTIAGO                                      *
*                                                                              *                                                                               
*          MATERIA:HERRAMIENTA DE PROGRAMACION WEB                             *
*                                                                              *
*          NUMERO DE CONTROL: 11161101                                         *
*                                                                              *
*                                                                              *
********************************************************************************/

/*******************************************************************************
*          ALUMNO:                                                             *
*          -CREATE                                                             *                                                                               
*          -UPDATE                                                             *
*          -DELETE                                                             *
*          -READ                                                               *
*                                                                              *
*                                                                              *
********************************************************************************/
var Alumno= function(
_clave,
_nombre, 
_apellidos,
_calificaciones
)

{
  return {
  "clave": _clave,
  "nombre": _nombre,
  "apellidos": _apellidos,
  "califcaciones": _calificaciones
  };
};


function asignarMateriasAlAlumno(g,m)
{
    if (!g.Alumno){
        g.Alumno = [];
}
if(!existeMateriasEnAlumno(g,m.clave)){
    g.Alumno.push(m);
    return true;
}
    return false;
}


function deleteAlumno ( g, clave )
{
   for (var i = 0; i<g.Alumno.length; i++) {
       if (g.Alumno[i]['clave'] === clave){
           g.Alumno.splice(i,1);
            return g.Alumno
        }
    }
    return 0;
}

function existeMateriasEnAlumno( g,cm)
{
  if (!g.Alumno || g.Alumno.length===0){
        return false;
}
    for (var i=0; i<g.Alumno.length; i++){
        if(g.Alumno[i].clave===cm){
            return true;
        }
    }
    return false;
}

function Bu_Alumno(al)
{
   for(var i = 0; i<Alumno.length; i++){
     if(Alumno[i].clave==al){
       return i;
     }
   }
   return -1;
 }

function updtAlumno(Alumno,clave,nombre,apellidos,calificaciones)
{
 Alumno.clave=clave;
 Alumno.nombre=nombre;
 Alumno.apellidos=apellidos;
 Alumno.califaciones=calificaciones;

}




/*******************************************************************************
*          GRUPO :                                                             *
*          -CREATE                                                             *                                                                               
*          -UPDATE                                                             *
*          -DELETE                                                             *
*          -READ                                                               *
*                                                                              *
*                                                                              *
********************************************************************************/


var Grupo= function(
  _clave,
   _nombre,
    _docentes,
     _alumnos, 
     _materias
     )

{
	return {
	"clave": _clave,
	"nombre": _nombre,
	"Docente": _docentes,
	"Alumno": _alumnos,
	"Materia": _materias
	};
};


function asignarMateriasAlGrupo(g,m){
    if (!g.Materia){
        g.Materia = [];
}
if(!existeMateriasEnGrupo(g,m.clave)){
    g.Materia.push(m);
    return true;
}
    return false;
}



function asignarAlumnosAlGrupo(g,a){
    if (!g.Alumno){
        g.Alumno = [];
}
if(!existeAlumnosEnGrupo(g,a.clave)){
    g.Alumno.push(a);
    return true;
}
    return false;
}

function asignarDocentesAlGrupo(g,d){
    if (!g.Docente){
        g.Docente = [];
}
if(!existeDocentesEnGrupo(g,d.clave)){
    g.Docente.push(d);
    return true;
}
    return false;
}


function existeMateriasEnGrupo(g,cm){
  if (!g.Materia || g.Materia.length===0){
        return false;
}
    for (var i=0; i<g.Materia.length; i++){
        if(g.Materia[i].clave===cm){
            return true;
        }
    }
    return false;
}


function existeAlumnosEnGrupo(g,ca){
  if (!g.Alumno || g.Alumno.length===0){
        return false;
}
    for (var i=0; i<g.Alumno.length; i++){
        if(g.Alumno[i].clave===ca){
            return true;
        }
    }
    return false;
}

function existeDocentesEnGrupo(g,cd){
  if (!g.Docente|| g.Docente.length===0){
        return false;
}
    for (var i=0; i<g.Docente.length; i++){
        if(g.Docente[i].clave===cd){
            return true;
        }
    }
    return false;
}

function deleteGrupo(g)
{

g.splice(0,0);

}

/*******************************************************************************
*          DOCENTE:                                                            *
*          -CREATE                                                             *                                                                               
*          -UPDATE                                                             *
*          -DELETE                                                             *
*          -READ                                                               *
*                                                                              *
*                                                                              *
********************************************************************************/


var Docente= function(
  _clave,
   _nombre, 
   _apellidos,
    _gradoacademico)
{
	return {
	"clave": _clave,
	"nombre": _nombre,
	"apellidos": _apellidos,
	"gradoacademico": _gradoacademico
	};
};


function asignarMateriasAlDocente(g,m){
    if (!g.Docente){
        g.Docente = [];
}
if(!existeMateriasEnAlumno(g,m.clave)){
    g.Docente.push(m);
    return true;
}
    return false;
}

function existeMateriasEnDocente(g,cm){
  if (!g.Docente || g.Docente.length===0){
        return false;
}
    for (var i=0; i<g.Docente.length; i++){
        if(g.Docente[i].clave===cm){ 
            return true;
        }
    }
    return false;
}

function Bu_Docente(cd){
   for(var i = 0; i<Docente.length; i++){
     if(Docente[i].clave==cd){
       return i;
     }
   }
   return -1;
 }
 
function updtDocente(Docente,clave,nombre,apellidos,grado_academico)
{
 
  Docente.clave=clave;
  Docente.nombre=nombre;
  Docente.apellidos=apellidos;
  Docente.grado_academico=grado_academico;
}


function EliminarDocente (g,cd){
   for (var i = 0; i<g.Docente.length; i++) {
       if (g.Docente[i]['clave'] === cd){
           g.Docente.splice(i,1);
            return g.Docente
        }
    }
    return 0;
}



/*******************************************************************************
*          MATERIA:                                                            *
*          -CREATE                                                             *                                                                               
*          -UPDATE                                                             *
*          -DELETE                                                             *
*          -READ                                                               *
*                                                                              *
*                                                                              *
********************************************************************************/

var Materia= function(
  _clave, 
  _nombre)
{

	return {
	"clave": _clave,
	"nombre": _nombre
	};
};


function Bu_Materia(cm){
   for(var i = 0; i<Materia.length; i++){
     if(Materia[i].clave==cm){
       return i;
     }
   }
   return -1;
 }
 
function updtMateria(Materia,clave,nombre)
{

  Materia.clave=clave;
  Materia.nombre=nombre;
}

function EliminarMateria (g,cm){
   for (var i = 0; i<g.Materia.length; i++) {
       if (g.Materia[i]['clave'] === cm){
           g.Materia.splice(i,0);
            return g.Materia;
        }
    }
    return 0;
}
/******************************************************************/

/***************************************************************/
/*     pruebas con alumno                                      */

var alumno1= Alumno("TOSG11","GERMAN","TORRES SANTIAGO","90");
var alumno2= Alumno("TOSG12","DAVID","LOPEZ SANTIAGO","100");
var alumno3= Alumno("TOSG13","JORGE","TORRES LOPEZ","80");
var docente1= Docente("AS1","JORGE","PEREZ","DR");
var docente2= Docente("AS2","BORO","LOPEZ","MR");
var docente3= Docente("AS3","LUIS","MONTES","MITI");
var materia1= Materia("AS1D","PROGRMACION ORIENTADA A OBJETOS");
var materia2= Materia("A2DAS","PROGRAMACION WEB");
var grupo1= Grupo("A1","ISA");
asignarAlumnosAlGrupo(grupo1,alumno1);
asignarAlumnosAlGrupo(grupo1,alumno2);
asignarDocentesAlGrupo(grupo1,docente2);
asignarMateriasAlGrupo(grupo1,materia1);
asignarMateriasAlGrupo(grupo1,materia2);
grupo1;



  
